import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;



public class XMLParser {
	static Document doc = null;
	
	
	@SuppressWarnings("null")
	public void parseXML(File parseFile) throws IOException	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		dbf.setCoalescing(true);
		dbf.setIgnoringElementContentWhitespace(true);
		dbf.setIgnoringComments(true);
		DocumentBuilder db =null;
		String manu = "";
		String model = "";		
		String attribute_name = "No_Name";		
		String attribute_value = "No_Value";
		String basic_description = "";
		String component="";
		String filePath="";
		GetAttributes handsetAttributes = new GetAttributes();
		dmConfig dmconfigdata =new dmConfig();
		GetTagValues getNodeValue =new GetTagValues();
		commandXmls commandXMLdata =new commandXmls();
		HandSetCharectristics getHandsetCharectristics=new HandSetCharectristics();
		DDFCharacteristics getDdfCharacteristics=new DDFCharacteristics();
		HandsetServices getHandsetXml=new HandsetServices();
		CommandXMLServices getCommandXmlServices=new CommandXMLServices();
		
		String component2="";
		String delimeter="#";
		//System.out.println("Delimeter : " + delimeter);
		filePath=parseFile.getParent();
		filePath=filePath.replace('\\', '#');
		filePath=filePath.replace(':', '#');
		String[] temp;
		 temp = filePath.split(delimeter);
		 int len=temp.length;
		 len=len-1;
		 component2=temp[len];
		 component=component2;
		 
		 
		//System.out.println("Folder Name for the file: ");
		
		writeData wr = new writeData();
		GetTagValues NT = new GetTagValues();
		try {
			db = dbf.newDocumentBuilder();
			doc=db.parse(parseFile);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("INFO: Parsing Completed "+component);
		
		switch (component) {
        case "DmConfigs": 
        	dmconfigdata.extractDmConfigParameters(doc);        	
		    break;
        case "CommandXmls": 
        	commandXMLdata.extractCommandXmlParameters(doc);
		    break;
		    
        case "HandsetServices": 
        	getHandsetXml.extractHandsetXmlParameters(doc);
		    break;
		    
        case "CommandXmlServices": 
        	//System.out.println("Going to getCommandXmlServices");
        	getCommandXmlServices.extractCommandXmlServiceParameters(doc);
		    break;
	 case "DDFGroups":
        	
        	System.out.println("Getting characteristics of DDF folder");
        	getDdfCharacteristics.getDDFCharacteristics(doc);
		    break;  	    
		    
          
        default: 
        	component="NotRequired";
        	
        	getHandsetCharectristics.getHandsetCharectristics(doc);        	
        	break;
		
		}		
	}
}
