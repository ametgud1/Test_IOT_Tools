import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Iterator;




public class IOTBLSComparator {
	
	static Hashtable<String, String> IOTTable = new Hashtable<String,String>();
	static Hashtable<String, String> BLSTable = new Hashtable<String,String>();

	public static void ReadFiles ( File File1, File File2 ) throws IOException
	{
		System.out.println("Reading input files!!!");
		String IOTline = null;
		String BLSline = null;
			
		String IOTKey = null;
		String BLSKey = null;
		
		String IOTValue = null;
		String BLSValue = null;
		
		
		//Reading the input IOTfile specified 
		
		if(File1.exists())
		{
			System.out.println("Checking for file existance!!!" + File1.getName());
			FileReader IOTFr = new FileReader(File1);
			BufferedReader IOTBr = new BufferedReader(IOTFr);
			while ((IOTline = IOTBr.readLine() )!= null )
			{				
				String [] tokens = IOTline.split(",", -1);
				System.out.println(IOTline);
				String ConcatenatedIOTKey ="";
				for(int i = 0 ; i<=((tokens.length)-3);i++){
					IOTKey = tokens[i];
					if(ConcatenatedIOTKey.equalsIgnoreCase("")){
						ConcatenatedIOTKey = IOTKey;
					}
					else{
						ConcatenatedIOTKey = ConcatenatedIOTKey+"," +IOTKey;
					}
				}
				
                for(int j = 0 ; j<=(tokens.length);j++)
                {
                	if (j==4) 
                	{
                		IOTValue=tokens[j];
                		//System.out.println("Trail : " + IOTValue);
                	}
                }
               // System.out.println("IOT Table Key : " + ConcatenatedIOTKey + " for value : " + IOTValue);
                IOTTable.put(ConcatenatedIOTKey,IOTValue);
                
			}
			//System.out.println();
			IOTBr.close();
			IOTFr.close();
		}
		else 
		{
			System.out.println("File : " + File1 + " not found.");
		} 

	    if(File2.exists())
	    {
	    	//Reading the input BLSfile specified 
	    	
			FileReader BLSFr = new FileReader(File2);
			BufferedReader BLSBr = new BufferedReader(BLSFr);
			
			while ((BLSline = BLSBr.readLine() )!= null )
			{
				String ConcatenatedBLSKey = "";
				String [] tokens2 = BLSline.split(",", -1);
				for(int i = 0 ; i<=((tokens2.length)-3);i++){
					IOTKey = tokens2[i];
					if(ConcatenatedBLSKey.equalsIgnoreCase("")){
						ConcatenatedBLSKey = IOTKey;
					}
					else{
						ConcatenatedBLSKey = ConcatenatedBLSKey+"," +IOTKey;
					}
					
				}				
			     for(int j = 0 ; j<=(tokens2.length);j++)
	                {
	                	if (j==4) 
	                	{
	                		
	                		BLSValue=tokens2[j];
	                		System.out.println("Check : " + BLSValue);
	                		
	                	}
	                }

			    // System.out.println("BLS Table Key : " + ConcatenatedBLSKey + " for value : " + BLSValue);
				BLSTable.put(ConcatenatedBLSKey,BLSValue);
			}
			
	    }
	    else
	    {
	    	System.out.println("File : " + File2 + " not found.");
	    }
	
	}

	public static void CompareContent(Hashtable<String, String> IOTHashTable,Hashtable<String, String> BLSHashtable,File outputfile) throws IOException
	{
		//Comparing File2 content against file1
		if(outputfile.exists())
		{
			System.out.println("File : " + outputfile + "Exists");
		}
		else
		{
		FileWriter fwr = new FileWriter(outputfile);
		System.out.println("Generating outputfile : " + outputfile.getName());
		
		
		
		BufferedWriter bwr = new BufferedWriter(fwr);
		boolean keyFount=false;
		 for (String key1 : IOTHashTable.keySet()){
			 	boolean blnExists = BLSHashtable.containsKey(key1);
			 	
			 	if(blnExists==true){
			 //	System.out.println(key1 + "  ...exists ");
			 	if( (IOTHashTable.get(key1)) .equals(BLSHashtable.get(key1))  )
                {
                //System.out.println(IOTHashTable.get(key1)+ " matched for " + key1);
               // System.out.println(key1+","+IOTHashTable.get(key1)+ ",PASS ,Same entries in both the files");
                bwr.write(key1+","+IOTHashTable.get(key1)+ ",\"PASS\" ,\"Same entries in both the files\"\n" );
                //System.out.println("Jaganmatha");
                
                }
			 	else
			 	{
			 		//System.out.println(IOTHashTable.get(key1) + " does not match for  :" + key1);
			 		//System.out.println(key1+","+IOTHashTable.get(key1)+",FAIL ,Key matches but different values in both the files");
			 		bwr.write(key1+","+IOTHashTable.get(key1)+",FAIL ,Key matches but different values in both the files\n");
			 	}

			 	}
			 	else{
			 		//System.out.println(key1 + "  ...NOOOOOT exists ");
			 		//System.out.println(key1+","+IOTHashTable.get(key1)+",Key not found in second file");
			 		bwr.write(key1+","+IOTHashTable.get(key1)+",Key not found in second file\n");
			 	}

				}
		// System.out.println("Searching for BLS Keys which are not present in IOT Table");
		 //System.out.println("=========================================================");
		 for (String key2 : BLSHashtable.keySet())
		 {
			 //System.out.println("Keys from BLS Table  :" + key2);
			
			 
			 boolean iotExists = IOTHashTable.containsKey(key2);
			// System.out.println("Checking the presense of BLS Key in IOT Key : " + iotExists);
			 if(iotExists==true) {
				//System.out.println(key2 + " ---present in IOT Table"); 
			 }
			 else{
				// System.out.println(key2+" ---is not presnt in IOT Table");
				// System.out.println(key2+","+BLSHashtable.get(key2)+",Key not found in first file");
				 bwr.write(key2+","+BLSHashtable.get(key2)+",Key not found in first file\n");
			 }
		 }
		
		 bwr.flush();
		}
		 
			}	
			
	public static void comparisonResult(File foldername) throws IOException	
	{
		//System.out.println("inside new function");
		//System.out.println("input folder : " +  foldername);
		File inputFile1 = null;
		File inputFile2=null;
		File outputfile=null;
		File AllFiles [] = foldername.listFiles();
		
		ArrayList<String> csvFileList = new ArrayList<String>();		
		//System.out.println("Length : " + AllFiles.length);
		for (File names : AllFiles){
			//System.out.println( names);
			
			if(names.isFile() && names.getName().endsWith(".csv") && names.getName().contains("DevicePack") )
			{
				csvFileList.add(names.getName());		       
			}
		}
		//System.out.println("Printing csv file list" );
		
		//System.out.println("Checking the size of file : "+ csvFileList.size());
	
		for (int i=0;i<csvFileList.size();i++){
	
			for(int j=i+1;j<csvFileList.size();j++){
				File f1 = new File(foldername+"/"+csvFileList.get(i));
				File f2 = new File(foldername+"/"+csvFileList.get(j));
				//System.out.println("comparing "+f1.getName()+" "+f2.getName());
				ReadFiles(f1, f2);
				
				String out1 = f1.getName().replaceAll("DevicePack_","").replaceAll("DM_config_Param_Values", "").replaceAll("_.csv", "");//+ f1.getName().replaceAll("DM_config_Param_Values", "");
				String out2 = f2.getName().replaceAll("DevicePack_", "").replaceAll("DM_config_Param_Values", "").replaceAll("_.csv", "");//+ f1.getName().replaceAll("DM_config_Param_Values", "");
				//System.out.println("File name : " + out1);
				//System.out.println("File name : " + out2);
				outputfile=new File(foldername+ "//" + out1 + "_VS_" + out2 + ".csv");
				System.out.println("Checking output file : " + outputfile.getName());
				
				
				
				
				
				
				if(!(outputfile.exists()))
				{
					outputfile.createNewFile();
					
				}
				else
				{
					outputfile.delete();
					//System.out.println("Output file " + outputfile.getName() + " exists , Hence deleting");
				}
				CompareContent(IOTTable, BLSTable,outputfile);
			}
			
	
			
		}
		
		//CompareContent(IOTTable, BLSTable);
		
		
		
		
	}
	
	
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

String foldername="C://Users//riyav//Desktop//IOT//ReleaseContent//Alcatel-5058O[TT3669]//PKG";
//System.out.println("Result Folder : " + foldername);
File inputfolder = new File("C://Users//riyav//Desktop//IOT//ReleaseContent//Alcatel-5058O[TT3669]//PKG");
File IOTFile = new File (foldername+"DM_config_Param_Values.csv");
File BLSFile = new File (foldername + "Handset_Param_Values.csv");

//File ComparisionResult = new File(foldername+"ComparisionResult.csv");
//if((ComparisionResult.exists()))
//{
//		ComparisionResult.delete();
//	    ComparisionResult.createNewFile();
//}
//else{
//	ComparisionResult.createNewFile();
//}
//		//ReadFiles(IOTFile , BLSFile);
////        CompareContent(IOTTable, BLSTable);
        comparisonResult(inputfolder);
	}

}
