import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.xml.parsers.ParserConfigurationException;



public class DevciePackageExtractor {
	 //Variable Declaration
	//UnZip flUnZip = new UnZip();
	public static String manuName;
	public static String modelName;
	static File folderName = null;
	static XMLParser parseFile = new XMLParser();
	static String contentToWrite;
	static String dmOutPutFileName;
	static String handsetOutPutFileName;
	
	//Main Starts here
	public static void main(String[] args) throws ParserConfigurationException, IOException {
		
		FileUnJar fileForUnJar=new FileUnJar();
		//System.out.println("Jaganmatha");
		if(args.length == 0){
			System.out.println("ERROR: Please select a folder. Exiting now...");
			System.exit(0);			
		}		
		
		//checking for folder
		if(args.length == 0){
			System.out.println("ERROR: Please select a folder. Exiting now...");
			System.exit(0);
			
		}
		folderName = new File(args[0]);		

		//Geting the list of files
		if(!(folderName.exists())){
			System.out.println("ERROR: Please check the path, " + folderName + " Not Exist.");
			System.exit(0);
		}
		File[] folderList = folderName.listFiles() ;

		//sorting the folder list
		Arrays.sort(folderList);
		for(int jarFilecount=0;jarFilecount<folderList.length;jarFilecount++){
			File fileName = folderList[jarFilecount].getAbsoluteFile();
			File newFolderName = null;
			if( fileName.getName().contains(".jar") ){				
				System.out.println("INFO: Unjaring the file " + fileName.toString()  );
				System.out.println("This will take time. Please be patience ");
				FileUnJar.unJarFile(fileName);
				reCreateOutputFiles(fileName);
				newFolderName=new File(fileName.toString().replace(".jar", ""));
				//System.out.println("filename : " + newFolderName.listFiles().length);
				//System.out.println("list of files : " );
				traversFiles(newFolderName.listFiles());
			}
		}
		
		System.out.println("Done....");
		//traversFiles(folderList);	
	}
	public static void traversFiles(File[] folderList) throws IOException{
		
		for (int z=0;z<folderList.length;z++){	
			//System.out.println("Iteration : " + z);
			System.out.println("INFO: Working on : " + folderList[z].getName());

			File fileName = folderList[z].getAbsoluteFile();
			if(fileName.isDirectory()){
				System.out.println("INFO: " + fileName.getName() + " is a directory");
				//System.exit(0);
				traversFiles(fileName.listFiles());
			}
			else if (fileName.isFile()){
				if(fileName.getName().contains(".xml")){
					//System.out.println("File Name : " + fileName);
					System.out.println("INFO: " + fileName.getName() + " is an XML file");
					parseFile.parseXML(fileName);
			
				}				
				else{
					System.out.println("WARNING: " + fileName.getName() + " is not a valid file");
				}
			}
		}
	}
	static void reCreateOutputFiles(File createFile){
		//System.out.println("Came to createFile " + createFile.getName());
		String fileName=createFile.getName().replace(".jar", "");
		String filePath=createFile.getParent();
		
		
		dmOutPutFileName=filePath + "\\"  + fileName + "_DM_config_Param_Values.csv";
		File f1=new File(dmOutPutFileName);

		handsetOutPutFileName=filePath + "\\" + fileName + "_Handset_Param_Values.csv";
		File f2=new File(handsetOutPutFileName);
		
		//Delete output file if exist
		if(f1.exists()){
			//System.out.println(" Checking for the file OverallTestCaseStatus.csv");
			f1.delete();
		}
		if(f2.exists()){
			f2.delete();
		}
		
		//writing header in output file
		writeData wr1 = new writeData();
		contentToWrite="Component,"+"Manu,"+"Model,"+"Param_name,"+"Param_value,"+"Basic_description"+""+'\n';
		wr1.FinalDataWriter(filePath + "\\"   + fileName + "_DM_config_Param_Values.csv",contentToWrite );
		wr1.FinalDataWriter(filePath + "\\"   + fileName + "_Handset_Param_Values.csv",contentToWrite );
	}
}
