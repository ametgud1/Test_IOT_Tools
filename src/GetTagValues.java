/**
 * Class: GetTagValues
 * @author Ramesh Kottakki
 * Description : This is to get the value of an XML Tag with its value.
 * This class is different than GetAttibutes which collects the attributes and current class collects the value of the Tag
 * The Inputs are the document, required Tag from which the attributes needs to extracted and component to specify it into output file 
 */
	import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class GetTagValues {
		public void getParamValues(Document getValueDoc,String nodeName,String component){
			
			String nodeValue="Global";
			writeData wr = new writeData();
			String basic_description="";
			NodeList searchTagNodeList;

			
			searchTagNodeList=getValueDoc.getElementsByTagName(nodeName);
			
			try{
					
				for (int i = 0; i < searchTagNodeList.getLength(); i++) {
					nodeValue = getValueDoc.getElementsByTagName(nodeName).item(i).getTextContent();
					
					if(nodeValue.equalsIgnoreCase("Global")){
						System.out.println("INFO: Not writing to File");
					}
					else{
						String dataToWrite = "";
						dataToWrite=dataToWrite+"\"";
						dataToWrite=dataToWrite+component;			
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+DevciePackageExtractor.manuName;
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+DevciePackageExtractor.modelName;
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+nodeName;
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+nodeValue;
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+basic_description;
						dataToWrite=dataToWrite+"\"";
						dataToWrite=dataToWrite + "" + '\n';
						
						//System.out.println("DatatoWrite :" + dataToWrite);
						wr.FinalDataWriter(DevciePackageExtractor.handsetOutPutFileName,dataToWrite);
					}
					
	
				}
				
				
			}
			catch (Exception e){
				nodeValue = "Global2";
			}
		}
}
