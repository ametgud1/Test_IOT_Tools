import org.w3c.dom.Document;


public class commandXmls {
	public void extractCommandXmlParameters(Document cmdXmlDoc){
		String manu = "";
		String model = "";		
		String attribute_name = "No_Name";		
		String attribute_value = "No_Value";
		String basic_description = "";
		String component="CommandXML";
		String filePath="";
		writeData wr = new writeData();
		GetAttributes commandXmlAttributes = new GetAttributes();
		GetTagValues cmdXMLValues = new GetTagValues();
		
		try{
			//System.out.println("came to commandXmls");
			commandXmlAttributes.getAttributeValues(cmdXmlDoc,"command-xml-association","Command XML");
			cmdXMLValues.getParamValues(cmdXmlDoc, "service-name","Command XML");
			
			cmdXMLValues.getParamValues(cmdXmlDoc, "token-table","Command XML");
			cmdXMLValues.getParamValues(cmdXmlDoc, "smsc-header","Command XML");
			cmdXMLValues.getParamValues(cmdXmlDoc, "cmd-xml","Command XML");			
		}
		catch (Exception e){
			
		}
		 //Writing to file
			String dataToWrite = "";
			dataToWrite=dataToWrite+"\"";
			dataToWrite=dataToWrite+component;			
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+manu;
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+model;
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+attribute_name;
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+attribute_value;
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+basic_description;
			dataToWrite=dataToWrite+"\"";
			dataToWrite=dataToWrite + "" + '\n';
			
			//System.out.println("DatatoWrite :" + dataToWrite);
			wr.FinalDataWriter(DevciePackageExtractor.handsetOutPutFileName,dataToWrite);
			wr.FinalDataWriter(DevciePackageExtractor.dmOutPutFileName,dataToWrite);
	}
}
