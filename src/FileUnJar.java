
/**
 * Class: FileUnJar
 * @author Ramesh Kottakki
 * Description : This Class will take a jar file as input, Extact the jar file.
 * This will generate a output folder with same name and path of jar file without jar 
 */

import java.io.File;
import java.io.IOException;


public class FileUnJar {
	public static void unJarFile(File infile) throws IOException{
		
		String destdir ;
		
		java.util.jar.JarFile jarFile = new java.util.jar.JarFile(new java.io.File(infile.toString())); //jar file path needs to mention here
	    java.util.Enumeration<java.util.jar.JarEntry> enu= jarFile.entries();
	    
	    //System.out.println("unJaring " + infile.getName() );
	    
	    while(enu.hasMoreElements())
	    {
	    	destdir = infile.toString().replace(".jar", "");    //This is the destination folder. The folder name is equal to jar file name without extention.
	        java.util.jar.JarEntry je = enu.nextElement();
	        
	        System.out.print("."); // Just printing a dot to show the progress
	        java.io.File fl = new java.io.File(destdir, je.getName().replace(':', '_'));
	        if(!fl.exists())
	        {	
	        	
	        	/*
	        	 * if file or folder is not exist then creating it.
	        	 */
	            fl.getParentFile().mkdirs();
	            fl = new java.io.File(destdir, je.getName().replace(':', '_'));
	        }
	        if(je.isDirectory())
	        {
	            continue;
	        }
	        java.io.InputStream is = jarFile.getInputStream(je);
	        java.io.FileOutputStream fo = new java.io.FileOutputStream(fl);
	        while(is.available()>0)
	        {
	            fo.write(is.read());
	        }
	        fo.close();
	        is.close();
	    }
	}

}
