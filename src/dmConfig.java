import java.io.File;

import org.w3c.dom.Document;


public class dmConfig {
	
	public void extractDmConfigParameters(Document dmDoc){
		String manu = "";
		String model = "";		
		String attribute_name = "No_Name";		
		String attribute_value = "No_Value";
		String basic_description = "";
		String component="DmConfigs";
		String filePath="";
		writeData wr = new writeData();
		try{
			
			try{
				manu = dmDoc.getElementsByTagName("manu-name").item(0).getTextContent();
			}
			catch (Exception e){
				manu = "Global";
			}

			try{
				model=dmDoc.getElementsByTagName("model-name").item(0).getTextContent() ;
			}
			catch (Exception e){
				model="Global";
			}
			
			try{
				attribute_name = dmDoc.getElementsByTagName("param-name").item(0).getTextContent();
			}
			catch (Exception e){
				attribute_name = "No_Name";
			}
			
			try{
				attribute_value = dmDoc.getElementsByTagName("param-value").item(0).getTextContent();
				//attribute_value=attribute_value.replace(',', '#');
			}
			catch (Exception e){
				attribute_value = "No_Value";
			}
			
			try{
				basic_description = dmDoc.getElementsByTagName("basic-description").item(0).getTextContent();
			}
			catch (Exception e){
				basic_description = "";
			}			
		}
		catch (Exception e){
			
		}
		 //Writing to file
			String dataToWrite = "";
			dataToWrite=dataToWrite+"\"";
			dataToWrite=dataToWrite+component;			
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+manu;
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+model;
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+attribute_name;
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+attribute_value;
			dataToWrite=dataToWrite+"\""+",";
			
			dataToWrite=dataToWrite+"\"";			
			dataToWrite=dataToWrite+basic_description;
			dataToWrite=dataToWrite+"\"";
			dataToWrite=dataToWrite + "" + '\n';
			
			//System.out.println("DatatoWrite :" + dataToWrite);
			wr.FinalDataWriter(DevciePackageExtractor.handsetOutPutFileName,dataToWrite);
			wr.FinalDataWriter(DevciePackageExtractor.dmOutPutFileName,dataToWrite);
	}

}
