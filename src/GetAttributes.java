
/**
 * Class: GetAttributes
 * @author Ramesh Kottakki
 * Description : This is to get all attributes of an XML Tag with its value.
 * The Inputs are the document, required Tag from which the attributes needs to extracted and component to specify it into output file 
 */

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;


public class GetAttributes {
	
	public void getAttributeValues(Document documentOfHandSet,String searchXMLTag,String component ){
		NodeList handsetObject;
		String attribute_name="";
		String attribute_value="";
		String basic_description="";
		writeData wr = new writeData();
		
		
		try{
			handsetObject = documentOfHandSet.getElementsByTagName(searchXMLTag);
			
			//Getting Manu Details
			try{
				/*
				 * Getting the Manu name and storing it in globla variable to use it in all transactions
				 */
				DevciePackageExtractor.manuName = documentOfHandSet.getElementsByTagName("manufacturer").item(0).getTextContent();
			}
			catch (Exception e){
				//Nothing
			}
			
			//Getting Model Details
			try{
				/*
				 * Getting the Model name and storing it in globla variable to use it in all transactions
				 */
				
				DevciePackageExtractor.modelName = documentOfHandSet.getElementsByTagName("model").item(0).getTextContent();
			}
			catch (Exception e){
				//Nothing
			}
			
			
			/*
			 * Check if the given tag has attributes. If one/more attributes available get the attribute name and value.
			 */
			
			if(handsetObject.getLength()!=0){
				NamedNodeMap attrs = handsetObject.item(0).getAttributes(); 
				for (int i=0;i<attrs.getLength();i++){
					
					 Attr attribute = (Attr)attrs.item(i);					
					 attribute_name=attribute.getName();
					 attribute_value=attribute.getValue();
					 
					 /*
					  * Collected data will be pushed to output file immediately
					  */
					 
						String dataToWrite = "";
						dataToWrite=dataToWrite+"\"";
						dataToWrite=dataToWrite+component;			
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+DevciePackageExtractor.manuName;
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+DevciePackageExtractor.modelName;
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+attribute_name;
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+attribute_value;
						dataToWrite=dataToWrite+"\""+",";
						
						dataToWrite=dataToWrite+"\"";			
						dataToWrite=dataToWrite+basic_description;
						dataToWrite=dataToWrite+"\"";
						dataToWrite=dataToWrite + "" + '\n';
						
						//System.out.println("DatatoWrite :" + dataToWrite);
						wr.FinalDataWriter(DevciePackageExtractor.handsetOutPutFileName,dataToWrite);
					 
				}
			}		
		}
		catch (Exception e){
			//handsetObject = "Not a Handset XML";
			//System.out.println("Nothing");
		}
	}
}
